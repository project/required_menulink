This module enables you to require a menu link for content types

It's also possible to only open the menu link by default but not require
it.

Drupal core automatically sets the menu link title to the node title
initially. This behavior can be overriden with this module. This can be
particulary useful to make the user take note of the required menu link,
which might otherwise just slip through.

## Configuration
Each content type can be configured to require a menu link at
admin/structure/types/manage/[content_type]. An additional setting
(vertical) tab can be found there named "Menu link settings"