/**
 * disable the automatic menu title provided by core
 * see @core/modules/menu_ui/menu_ui.js
 */
(function ($, Drupal) {
  Drupal.behaviors.menuUiLinkAutomaticTitleDisabling = {
    attach: function attach(context) {
      var $context = $(context);
      $context.find('.menu-link-form').each(function () {
        var $this = $(this);
        var $linkTitle = $context.find('.js-form-item-menu-title input');

        $linkTitle.data('menuLinkAutomaticTitleOverridden', true);

      });
    }
  };
})(jQuery, Drupal);