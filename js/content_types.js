/**
 * Replicate behavior from node/drupal.content_types library to dynamically
 * update the vertical tabs element with information about selected options
 */
(function ($, Drupal) {
  Drupal.behaviors.contentTypesRequiredMenulink = {
    attach: function attach(context) {
      var $context = $(context);
      $context.find('#edit-require-menu').drupalSetSummary(function (context) {
        var vals = [];
        var $editContext = $(context);
        $editContext.find('input:checked').next('label').each(function () {
          vals.push(Drupal.checkPlain($(this).text()));
        });

        return vals.join(', ');
      });
    }
  };
})(jQuery, Drupal);